package com.sarunpat.week8;

public class TestMap {
    private int width;
    private int height;

    public TestMap(int width, int height){
        this.width = width;
        this.height = height;
    }

    public TestMap(){
        this.width = 5;
        this.height = 5;
    }

    public void printMap() {
        for(int i = 0; i< height; i++){
            for(int j = 0; j<width;j++){
                System.out.print("-");
            }
            System.out.println();
        }
    }
}
