package com.sarunpat.week8;

public class CircleApp {
    public static void main(String[] args) {
        Circle circle1 = new Circle("Circle1",1);
        circle1.printCircleArea();
        circle1.printCirclePerimeter();
        Circle circle2 = new Circle("Circle2",2);
        circle2.printCircleArea();
        circle2.printCirclePerimeter();
    }
}
