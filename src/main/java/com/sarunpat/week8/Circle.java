package com.sarunpat.week8;

public class Circle {
    private String name;
    private double radius;

    public Circle(String name,double radius){
        this.name = name;
        this.radius = radius ;
    }

    public double printCircleArea() {
        double area = 3.14 * (radius * radius);
        System.out.println(name + " area = " + area);
        return area;
    }

    public double printCirclePerimeter() {
        double perimeter = 2 * 3.14 * radius;
        System.out.println(name + " perimeter = " + perimeter);
        return perimeter;
    }

    
}
