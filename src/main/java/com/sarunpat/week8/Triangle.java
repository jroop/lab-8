package com.sarunpat.week8;
import java.lang.Math;
public class Triangle {
    private int a;
    private int b;
    private int c;

    public Triangle(int a , int b , int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double printTriangleArea() {
        double s = (a+b+c)/2;
        double area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
        System.out.println("Triangle area = " + area);
        return area;
    }

    public int printTrianglePerimeter() {
        int perimeter = a+b+c;
        System.out.println("Triangle perimeter = " + perimeter);
        return perimeter;
    }
}
